# Basic Ubuntu container to be used as the base for all our other containers.




This is a base container for all other [Ubuntu][1] containers. 


## To Build

```
> docker build --tag codered/ubuntu-base . # normal build
> docker build --no-cache=true --force-rm=true --tag codered/ubuntu-base. # force a full build
> docker push codered/ubuntu-base # send it to docker hub
```

## To Run

```
>  docker run -t -i codered/ubuntu-base


```

[1]:  http://www.ubuntu.com//

